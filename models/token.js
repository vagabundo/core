module.exports = (sequelize, DataTypes) => {
	const Token = sequelize.define('Token', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		payload: DataTypes.STRING
	})
	Token.associate = function(models) {
		Token.belongsTo(models.Blog)
		Token.hasOne(models.Provider)
	}

	return Token
}
