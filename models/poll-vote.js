module.exports = (sequelize, DataTypes) => {
	const PollVote = sequelize.define('PollVote', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		value: DataTypes.STRING
	})
	PollVote.associate = function(models) {
		PollVote.belongsTo(models.Poll)
		PollVote.belongsTo(models.User)
	}

	return PollVote
}
