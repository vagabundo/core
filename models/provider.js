module.exports = (sequelize, DataTypes) => {
	const Provider = sequelize.define('Provider', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		type: DataTypes.STRING,
		chat_id: DataTypes.STRING,
		group: DataTypes.BOOLEAN,
		state: DataTypes.STRING
	})
	Provider.associate = function(models) {
		Provider.belongsTo(models.User)
		Provider.belongsTo(models.Blog)
		Provider.belongsTo(models.Token)
		Provider.hasMany(models.ProviderLog)
	}

	return Provider
}
