const S3 = require('../helpers/s3')
const sleep = require('../helpers/sleep')

module.exports = (sequelize, DataTypes) => {
	const Item = sequelize.define('Item', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		date: DataTypes.DATE,
		content: DataTypes.TEXT,
		thumbnail: DataTypes.STRING,
		caption: DataTypes.STRING,
		type: DataTypes.STRING,
		fileSize: { type: DataTypes.INTEGER, field: 'file_size' },
		width: DataTypes.INTEGER,
		height: DataTypes.INTEGER
	})
	Item.associate = function(models) {
		Item.belongsTo(models.Blog)
		Item.belongsTo(models.User)
		Item.hasOne(models.ProviderLog)
		Item.hasMany(models.Reaction)
	}

	Item.afterCreate(async item => {
		const { Op } = sequelize
		const { Provider, TelegramQueue } = sequelize.models

		await item.update({ date: item.created_at })

		// notify followers async
		const notify = async () => {
			try {
				// debounce
				await sleep(process.env.NODE_ENV === 'production' ? 1000 * 60 * 5 : 0)
				const newerItem = await Item.find({
					where: {
						blog_id: item.blog_id,
						created_at: { [Op.gt]: item.created_at }
					}
				})
				if (newerItem) {
					// if there is a newer item end this function
					return
				}

				const blog = await item.getBlog()
				const itemUser = await item.getUser()

				const users = await blog.getUsers()

				for (const user of users) {
					const content = `📓 *${itemUser.name}* posted something on ${
						blog.subdomain
					}.vagabundo.co (*${blog.name}*)`
					await TelegramQueue.create({
						type: 'text',
						chat_id: user.telegramUserId,
						content
					})
				}
			} catch (error) {
				console.error(error)
			}
		}

		notify()
	})
	Item.afterDestroy(async item => {
		const fileTypes = ['image', 'video', 'audio']
		if (fileTypes.includes(item.type)) {
			await S3.delete(item.content)
		}
	})

	return Item
}
