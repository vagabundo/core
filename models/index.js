const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')

// Models
const models = [
	require('./blog-follower-token'),
	require('./blog-follower'),
	require('./blog'),
	require('./item'),
	require('./login'),
	require('./pageview'),
	require('./poll-vote'),
	require('./poll'),
	require('./provider-log'),
	require('./provider'),
	require('./reaction'),
	require('./telegram-queue'),
	require('./token'),
	require('./user')
]

const config = {
	logging: false,
	dialect: process.env.NODE_ENV === 'test' ? 'sqlite' : 'mysql',
	define: {
		underscored: true,
		underscoredAll: true
	}
}

const sequelize = new Sequelize(process.env.DATABASE_URL, config)
const db = {}

for (const modelGenerator of models) {
	const model = modelGenerator(sequelize, Sequelize)
	db[model.name] = model
}

Object.keys(db).forEach(modelName => {
	if ('associate' in db[modelName]) {
		db[modelName].associate(db)
	}
})

db.sequelize = sequelize
db.Sequelize = Sequelize

const dbSync = db.sequelize.sync({
	alter: false
})

dbSync.then(() => {
	console.log('[x] initialized DB')
})

db.initialized = dbSync

module.exports = db
