module.exports = (sequelize, DataTypes) => {
	const Pageview = sequelize.define(
		'Pageview',
		{
			id: {
				primaryKey: true,
				type: DataTypes.INTEGER,
				autoIncrement: true
			}
		},
		{
			updatedAt: false
		}
	)
	Pageview.associate = function(models) {
		Pageview.belongsTo(models.Blog)
	}

	return Pageview
}
