module.exports = (sequelize, DataTypes) => {
	const BlogFollower = sequelize.define('BlogFollower', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		userId: {
			type: DataTypes.INTEGER,
			field: 'user_id',
			unique: 'blog_user'
		},
		blogId: {
			type: DataTypes.INTEGER,
			field: 'blog_id',
			unique: 'blog_user'
		},
		accepted: { type: DataTypes.BOOLEAN },
		acceptedAt: { type: DataTypes.DATE, field: 'accepted_at' }
	})
	BlogFollower.associate = function(models) {
		BlogFollower.belongsTo(models.User)
		BlogFollower.belongsTo(models.Blog)
	}
	BlogFollower.afterCreate(async blogFollower => {
		const { Blog, User, Provider, TelegramQueue } = sequelize.models
		try {
			const user = await User.findById(blogFollower.userId)
			const blog = await Blog.findById(blogFollower.blogId)

			const providers = await Provider.findAll({
				where: {
					type: 'telegram',
					blog_id: blog.id
				}
			})

			if (blog.private) {
				// Private Blog
				const infoMessage = `*New Follower* 🔥

*${user.name}* wants to follow your blog *${
					blog.name
				}*. You can now grant or deny *${user.name}* access to your blog.`
				const options = {
					reply_markup: {
						inline_keyboard: [
							[
								{
									text: 'deny access 🚫',
									callback_data: JSON.stringify({
										type: 'follow-request',
										follower_id: blogFollower.id,
										accepted: false
									})
								},
								{
									text: 'grant access ✅',
									callback_data: JSON.stringify({
										type: 'follow-request',
										follower_id: blogFollower.id,
										accepted: true
									})
								}
							]
						]
					},
					caption: infoMessage
				}
				for (const provider of providers) {
					if (user.profileImage) {
						await TelegramQueue.create({
							type: 'image',
							chat_id: provider.chat_id,
							content: `https://cdn.vagabundo.co/${user.profileImage}`,
							options
						})
					} else {
						await TelegramQueue.create({
							type: 'text',
							chat_id: provider.chat_id,
							content: infoMessage,
							options
						})
					}
				}
				// End Private Blog
			} else {
				// Public Blog

				// Accept all blog followers when blog is public
				await blogFollower.update({ accepted: true, acceptedAt: new Date() })

				const message = `*New Follower* 🔥

*${user.name}* is now following your blog *${blog.name}*`
				for (const provider of providers) {
					if (user.profileImage) {
						await TelegramQueue.create({
							type: 'image',
							chat_id: provider.chat_id,
							content: `https://cdn.vagabundo.co/${user.profileImage}`,
							options: { caption: message }
						})
					} else {
						await TelegramQueue.create({
							type: 'text',
							chat_id: provider.chat_id,
							content: message
						})
					}
				}
				// End Public Blog
			}
		} catch (error) {
			console.error(error)
		}
	})

	return BlogFollower
}
