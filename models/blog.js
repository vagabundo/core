const nanoid = require('nanoid/generate')

const generateId = () => nanoid('abcdefghijklmnopqrstuvwxyz', 10)

module.exports = (sequelize, DataTypes) => {
	const Blog = sequelize.define('Blog', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		name: DataTypes.STRING,
		domain: DataTypes.STRING,
		subdomain: {
			type: DataTypes.STRING(100),
			defaultValue: generateId,
			unique: true,
			set(subdomain) {
				this.setDataValue('subdomain', subdomain.toLowerCase())
			}
		},
		profileImage: {
			field: 'profile_image',
			type: DataTypes.STRING
		},
		description: { type: DataTypes.STRING, defaultValue: '' },
		password: DataTypes.STRING,
		theme: DataTypes.STRING,
		private: DataTypes.BOOLEAN,
		map: DataTypes.BOOLEAN,
		discovery: DataTypes.BOOLEAN,
		showAuthor: { type: DataTypes.BOOLEAN, field: 'show_author' },
		newsletterSentAt: { type: DataTypes.DATE, field: 'newsletter_sent_at' }
	})
	Blog.associate = function(models) {
		Blog.hasMany(models.Item)
		Blog.belongsToMany(models.User, {
			through: { model: models.BlogFollower, unique: false }
		})
	}

	Blog.createWithName = function(name) {
		const options = {
			subdomain: `${name}-${generateId()}`,
			name: `${name || 'Vagabundo'}'s Blog`,
			description: `This is ${name || 'Vagabundo'}'s new Blog 🚀`
		}
		return Blog.create(options)
	}

	return Blog
}
