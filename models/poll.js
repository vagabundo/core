module.exports = (sequelize, DataTypes) => {
	const Poll = sequelize.define('Poll', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		name: DataTypes.STRING
	})
	Poll.associate = function(models) {
		Poll.hasMany(models.PollVote)
	}

	return Poll
}
