const reactions = require('../helpers/reactions')

module.exports = (sequelize, DataTypes) => {
	const Reaction = sequelize.define('Reaction', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		value: {
			type: DataTypes.STRING
		}
	})
	Reaction.associate = function(models) {
		Reaction.belongsTo(models.Item)
		Reaction.belongsTo(models.User)
	}
	Reaction.afterCreate(async reaction => {
		const { User, Item, Provider, TelegramQueue } = sequelize.models
		try {
			const user = await User.findById(reaction.user_id)
			const item = await Item.findById(reaction.item_id)

			const providerLog = await item.getProviderLog()
			if (providerLog) {
				// if ProviderLog was found. only notify this provider
				await TelegramQueue.create({
					type: 'text',
					chat_id: providerLog.chatId,
					reply_to_message_id: providerLog.messageId,
					content: `*${user.name}* reacted to your post with ${
						reactions[reaction.value]
					}`
				})
				return
			}

			const providers = await Provider.findAll({
				where: {
					type: 'telegram',
					blog_id: item.blog_id
				}
			})

			for (const provider of providers) {
				await TelegramQueue.create({
					type: 'text',
					chat_id: provider.chat_id,
					content: `*${user.name}* reacted to your post with ${
						reactions[reaction.value]
					}`
				})
			}
		} catch (error) {
			console.error(error)
		}
	})

	return Reaction
}
