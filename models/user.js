const generateAnimal = require('adjective-adjective-animal')
const sendMail = require('../helpers/email')

module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		name: DataTypes.STRING,
		email: {
			type: DataTypes.STRING(100),
			validate: {
				isEmail: true
			},
			set(value) {
				this.setDataValue('email', (value || '').trim().toLowerCase())
			},
			unique: true
		},
		telegramUserId: {
			type: DataTypes.STRING(100),
			field: 'telegram_user_id',
			unique: true
		},
		profileImage: { type: DataTypes.STRING, field: 'profile_image' }
	})
	User.associate = function(models) {
		User.hasMany(models.Provider)
		User.hasMany(models.Login)
		User.belongsToMany(models.Blog, {
			through: { model: models.BlogFollower, unique: false }
		})
	}
	User.findByTelegramUserId = function(userId) {
		return User.findOne({ where: { telegramUserId: userId } })
	}
	User.prototype.loginRequest = async function(userAgent) {
		const securityCode = await generateAnimal({
			adjectives: 1,
			format: 'title'
		})
		const {
			emailToken,
			browserToken,
			id
		} = await sequelize.models.Login.create({
			user_id: this.id,
			userAgent
		})

		try {
			// Send via Telegram
			const options = {
				reply_markup: {
					inline_keyboard: [
						[
							{
								text: '❌ Deny',
								callback_data: `{"type":"verifyLogin","id":${id}, "value": "deny"}`
							},
							{
								text: '✅ Verify',
								callback_data: `{"type":"verifyLogin","id":${id}, "value": "verify"}`
							}
						]
					]
				}
			}
			const providers = await this.getProviders()
			for (const provider of providers) {
				const content = `We have received a login attempt with the following code: 

*${securityCode}*

If you didn't attempt to log in but received this message, please deny the login.
If you are concerned about your account's safety, please message @officialfischer.`

				await sequelize.models.TelegramQueue.create({
					type: 'text',
					chat_id: provider.chat_id,
					content,
					options
				})
			}
		} catch (error) {
			console.error(error)
		}

		await sendMail({
			to: this.email,
			from: 'login@vagabundo.co',
			subject: `Vagabundo.co Login Verification (code: "${securityCode}")`,
			html: `
			<div class="container" style="padding: 30px;border: solid #aaa 1px;border-radius: 8px;">
				<div style="text-align:center;">
					<h1>Verify your email to log in to Vagabundo</h1
				</div>
				<div>We have received a login attempt with the following code:
				</div>
				<div style="margin: 20px 0;padding: 10px;background-color: #ddd;border-radius:8px;font-weight: bold;">${securityCode}
				</div>
				<div style="margin: 20px 0;text-align:center;">
					<a href="https://vagabundo.co/notifications/email-confirmed?email=${
						this.email
					}&token=${emailToken}&mode=login" style="border: none;padding: 10px;background-color: black;color: #fff;	border-radius: 8px;	cursor: pointer;font-weight: bold;margin: 0;	transition: background-color 0.15s ease-in;	text-decoration: none;">Verify</a>
				</div>
				<p style="color: #aaa;font-size: 0.8em;	margin-top:20px;">
					If you didn't attempt to log in but received this email, or if the location doesn't match, please ignore this email. If you are concerned about your account's safety, please reply to this email to get in touch with us.
				</p>
			</div>
			`
		})
		return { token: browserToken, securityCode }
	}

	return User
}
