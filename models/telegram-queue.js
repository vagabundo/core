module.exports = (sequelize, DataTypes) => {
	const TelegramQueue = sequelize.define('TelegramQueue', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		chat_id: DataTypes.STRING,
		reply_to_message_id: DataTypes.STRING,
		content: DataTypes.TEXT,
		type: DataTypes.STRING,
		options: {
			type: DataTypes.TEXT,
			set(value) {
				if (value) {
					this.setDataValue('options', JSON.stringify(value))
				}
			},
			get() {
				return JSON.parse(this.getDataValue('options'))
			}
		},
		deliveredAt: {
			type: DataTypes.DATE,
			field: 'delivered_at'
		},
		erroredAt: {
			type: DataTypes.DATE,
			field: 'errored_at'
		},
		error: DataTypes.TEXT
	})

	return TelegramQueue
}
