const nanoid = require('nanoid')
const sendMail = require('../helpers/email')

module.exports = (sequelize, DataTypes) => {
	const BlogFollowerToken = sequelize.define('BlogFollowerToken', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		name: DataTypes.STRING,
		email: {
			type: DataTypes.STRING,
			validate: {
				isEmail: true
			},
			set(value) {
				this.setDataValue('email', (value || '').trim().toLowerCase())
			}
		},
		token: {
			type: DataTypes.STRING,
			defaultValue: nanoid
		},
		verifiedAt: {
			type: DataTypes.DATE,
			field: 'verified_at'
		}
	})
	BlogFollowerToken.associate = function(models) {
		BlogFollowerToken.belongsTo(models.Blog)
	}

	BlogFollowerToken.prototype.sendMail = async function() {
		const blog = await this.getBlog()

		const activationLink = `https://${blog.subdomain}.vagabundo.co/follow/${
			this.token
		}`

		await sendMail({
			to: this.email,
			from: 'noreply@vagabundo.co',
			subject: `${blog.name} - Newsletter`,
			html: `
			<div class="container" style="padding: 30px;border: solid #aaa 1px;border-radius: 8px;">
				<div style="text-align:center;">
					<h1>${blog.name} - Newsletter</h1
				</div>
				<div>Click on the Link bellow to follow ${blog.name} <small>(${
				blog.subdomain
			}.vagabundo.co)</small>
				</div>
				<div style="margin: 20px 0;text-align:center;">
					<a href="${activationLink}" style="border: none;padding: 10px;background-color: black;color: #fff;	border-radius: 8px;	cursor: pointer;font-weight: bold;margin: 0;	transition: background-color 0.15s ease-in;	text-decoration: none;">${activationLink}</a>
				</div>
				<p style="color: #aaa;font-size: 0.8em;	margin-top:20px;">
					If you didn't attempt to follow this blog please ignore this email.
				</p>
			</div>
			`
		})
	}

	BlogFollowerToken.prototype.verify = async function() {
		const { User, BlogFollower } = sequelize.models

		let user = await User.findOne({ where: { email: this.email } })
		if (!user) {
			user = await User.create({ name: this.name, email: this.email })
		}

		try {
			await BlogFollower.create({ user_id: user.id, blog_id: this.blog_id })
		} catch (error) {
			console.error('BlogFollower already exists.')
		}

		await this.update({ verifiedAt: new Date() })
	}

	return BlogFollowerToken
}
