module.exports = (sequelize, DataTypes) => {
	const ProviderLog = sequelize.define('ProviderLog', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		content: DataTypes.TEXT,
		userId: { type: DataTypes.INTEGER, field: 'user_id' },
		chatId: { type: DataTypes.INTEGER, field: 'chat_id' },
		messageId: { type: DataTypes.INTEGER, field: 'message_id' }
	})
	ProviderLog.associate = function(models) {
		ProviderLog.belongsTo(models.Provider)
		ProviderLog.belongsTo(models.Item)
	}

	return ProviderLog
}
