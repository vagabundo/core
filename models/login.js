const nanoid = require('nanoid')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
	const Login = sequelize.define('Login', {
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true
		},
		browserToken: {
			type: DataTypes.STRING,
			field: 'browser_token',
			defaultValue: nanoid
		},
		emailToken: {
			type: DataTypes.STRING,
			field: 'email_token',
			defaultValue: nanoid
		},
		userAgent: { type: DataTypes.STRING, field: 'user_agent' }
	})
	Login.associate = function(models) {
		Login.belongsTo(models.User)
	}
	Login.findByToken = async function(token) {
		const payload = jwt.decode(token)
		if (!payload) throw new Error('no token provided')
		const login = await Login.find({ where: { id: payload.login } })

		return login
	}
	Login.prototype.getToken = function() {
		return jwt.sign(
			{
				login: this.id
			},
			process.env.SECRET || 'secret',
			{
				expiresIn: '1y'
			}
		)
	}

	return Login
}
