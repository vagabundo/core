const AWS = require('aws-sdk')
const generateId = require('nanoid')

const BUCKET_NAME = 'travelstream'

// Configuring the AWS environment
AWS.config.update({
	accessKeyId: process.env.S3_ACCESS_KEY_ID,
	secretAccessKey: process.env.S3_SECRET_ACCESS_KEY
})

const s3 = new AWS.S3()

module.exports = {
	async upload({ fileStream, mimeType, name, folder, id }) {
		if (!id) {
			id = generateId()
		}
		const params = {
			Bucket: BUCKET_NAME,
			Body: fileStream,
			Key: `${folder ? `${folder}/` : ''}${id}/${name}`,
			ContentType: mimeType
		}

		return new Promise((resolve, reject) => {
			s3.upload(params, (error, data) => {
				if (error) {
					return reject(error)
				}
				return resolve(data)
			})
		})
	},
	async delete(key) {
		return new Promise((resolve, reject) => {
			s3.deleteObject({ Bucket: BUCKET_NAME, Key: key }, (error, data) => {
				if (error) {
					return reject(error)
				}
				return resolve(data)
			})
		})
	}
}
