const mailgun = require('mailgun-js')({
	apiKey: process.env.MAILGUN_KEY,
	domain: 'mg.vagabundo.co',
	host: 'api.eu.mailgun.net'
})

const sendMail = options => {
	const mailOptions = Object.assign(options)
	mailOptions.from = options.from || 'noreply.vagabundo.co'

	return new Promise((resolve, reject) => {
		mailgun.messages().send(mailOptions, (error, body) => {
			if (error) return reject(error)
			resolve(body)
		})
	})
}

module.exports = sendMail
