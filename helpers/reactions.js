module.exports = {
	'clapping-hands': '👏',
	cry: '😢',
	'face-with-tears-of-joy': '😂',
	fire: '🔥',
	heart: '❤️',
	'open-mouth': '😮'
}
